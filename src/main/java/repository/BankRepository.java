package repository;

import domain.Bank;
import domain.Person;

public interface BankRepository extends BaseRepository<Bank, Long>  {

}
