package repository;

import java.util.Collection;

import domain.Account;
import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long>  {
	Sede findByAddress(String address);

	Collection<Sede> findBySedeId(Long bankId);
}
