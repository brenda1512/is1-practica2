package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AccountRepository;
import repository.SedeRepository;
import domain.Account;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
SedeRepository{

	@Override
	public Sede findByAddress(String address) {
		String jpaQuery = "SELECT a FROM Sede a WHERE a.address = :address";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("address", address);
		return getFirstResult(query);
	}

	@Override
	public Collection<Sede> findBySedeId(Long bankId) {
		String jpaQuery = "SELECT a FROM Sede a JOIN a.banks p WHERE p.id = :bankId";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("bankId", bankId);
		return query.getResultList();
	}

}
