package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.BankRepository;
import domain.Bank;


@Repository
public class JpaBankRepository extends JpaBaseRepository<Bank, Long> implements
BankRepository{

}
