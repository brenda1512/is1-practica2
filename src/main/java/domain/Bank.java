package domain;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "tbl_bank", indexes = { @Index(columnList = "number") })
public class Bank implements BaseEntity<Long>{
	@Id
	@SequenceGenerator(name = "bank_id_generator", sequenceName = "bank_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_id_generator")
	private Long id;


	@Column(length = 64)
	private String name;

	@Column(length = 64)
	private String address;
	
	@OneToMany
    @JoinTable(name="bank_sede")
	private Collection<Sede> sedes;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public Bank(String name, String address)
	{
		this.name		=	name;
		this.address	=	address;
	}
	

	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setName(String name)
	{
		this.name	=	name;
	}
	public void setAddress(String address)
	{
		this.address	=	address;
	}
		
	public Collection<Sede> getSedes() {
		return sedes;
	}

	public void setAccounts(Collection<Sede> sedes) {
		this.sedes = sedes;
	}

}
