package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "tbl_sede", indexes = { @Index(columnList = "number") })
public class Sede implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "sede_id_generator", sequenceName = "sede_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String address;
	
	@ManyToMany
    @JoinTable(name="bank_sede")
	private Collection<Bank> banks;
	
	public Sede() {
	}
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public Collection<Bank> getBanks() {
		return banks;
	}
	
	public void setBanks(Collection<Bank> banks) {
		this.banks = banks;
	}
	
	

}
